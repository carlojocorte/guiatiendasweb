$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover()
    $('.carousel').carousel({
        interval: 2000
    });
    $('#modal-contacto').on('show.bs.modal',function(e){
        console.log('Se está mostrando la modal');  
        $('#contacto-btn').removeClass('btn-outline-success');
        $('#contacto-btn').addClass('btn-primary');
        $('#contacto-btn').prop('disabled',true); 
    })
    $('#modal-contacto').on('shown.bs.modal',function(e){
        console.log('Se mostró la modal');   
    })
    $('#modal-contacto').on('hide.bs.modal',function(e){
        console.log('Se está ocultando la modal');   
    })
    $('#modal-contacto').on('hidden.bs.modal',function(e){
        console.log('Se ocultó la modal');   
        $('#contacto-btn').prop('disabled',false); 
    })
});